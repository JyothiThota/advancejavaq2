import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TreeMapStudent {

	public static void main(String[] args) {
		System.out.println("Question 2");
		TreeMap<Integer,String>tmap =new TreeMap<Integer,String>();
		tmap.put(5, "Ruby");
		tmap.put(4, "Bob");
		tmap.put(2, "Sid");
		tmap.put(1, "Katy");
		tmap.put(3, "Robert");
		Set set = tmap.entrySet();
		Iterator iterator = set.iterator();
		while(iterator.hasNext())
		{
			Map.Entry mpEntry = (Map.Entry)iterator.next();
			System.out.println("Registered number is : " +mpEntry.getKey() + " and Student Name is  : " +mpEntry.getValue());
		}
		
	}

}
