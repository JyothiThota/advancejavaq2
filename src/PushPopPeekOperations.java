import java.util.Stack;

public class PushPopPeekOperations {

	public static void main(String[] args) {
		System.out.println(" Question 1 ");
		
		Stack<String> names = new Stack<String>();
		
		System.out.println("The names are inserted using push() opeation in the stack ");
		names.push("Lubi");
		names.push("Mike");
		names.push("Ruby");
		names.push("John");
		names.push("Ellie");
		
		
		System.out.println("\n The names in the stack are : \n" +names);
		System.out.println("\n The element on the top of the Stack (peek()) is \n" +names.peek());
		
		System.out.println("\n The last name is popped out (pop()) is : \n" +names.pop());
		System.out.println("\n The elements in stack after pop : \n"+ names);
		
	}

}
