import java.util.Arrays;
import java.util.Scanner;
public class ReverseFirstThreeNumbers {

	public static void main(String[] args) {
		System.out.println("Question 9");
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the length of the array :");
		int length =sc.nextInt();
		int arr[] = new int[length];
		
		System.out.println("Enter the numbers : ");
		for(int i=0;i<arr.length;i++)
		{
			arr[i]=sc.nextInt();
		}
		
		System.out.println("Array before revesing : "+ Arrays.toString(arr));
		
		for(int i=0;i<arr.length;i=i+3)
		{
			if(i+2 >arr.length)
			{
				break;
			}
			
			int temp =arr[i];
			arr[i] = arr[i+2];
			arr[i+2] =temp;
		}
		
		System.out.println("Array after reversing is " +Arrays.toString(arr));
	}

}
