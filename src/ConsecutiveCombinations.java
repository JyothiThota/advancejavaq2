import java.util.Scanner;

public class ConsecutiveCombinations {

	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the number : ");
		
		int input =sc.nextInt();
		for(int j=1;j<input;j++)
		{
			int temp_sum =0;
			int[] temp_array =new int[input];
			for(int i=j;i<input;i++)
			{
				temp_array[i-1] = i;
				temp_sum = temp_sum+i;
				
				if(temp_sum == input)
				
					break;
				if(temp_sum >input)
					{
						temp_array =null;
						break;
					}
					
			}
			
			if(temp_sum == input)
			{
				System.out.println("Sum : " +temp_sum);
				
				for(int k:temp_array)
					
					if(k!=0)
					System.out.println(k);
			}
		}

	}

}
